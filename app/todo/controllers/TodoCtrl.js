'use strict';

angular.module('mhkAppApp')
    .controller('TodoCtrl', ['$scope', 'TodoService', function($scope, Service){
        $scope.totalTodos = 0;
        $scope.todos = [];
        $scope.getTotalTodos = function() {
            return $scope.todos.length;
        }

        $scope.addTodo = function() {
            Service.addItem($scope.todos, {text:$scope.formTodoText, done:false});
            $scope.formTodoText = '';
        }

        $scope.delTodo = function() {
            Service.delItem($scope.todos, this.$index);
        }
    }]);
