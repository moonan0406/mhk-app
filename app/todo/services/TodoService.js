angular.module('mhkAppApp')
    .factory('TodoService', function(){
        var addItem = function(items, item){
            items.push(item);
        };

        var delItem = function(items,item){
            items.splice(item, 1);
        };

        return {
            addItem: addItem,
            delItem: delItem
        };
    });