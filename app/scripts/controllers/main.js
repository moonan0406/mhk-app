'use strict';

/**
 * @ngdoc function
 * @name mhkAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mhkAppApp
 */
angular.module('mhkAppApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
